# Weather Mobile App

## Architecture

Clean Architecture:

- App (main entry point, DI)
- Domain (domain layer module)
- Features (presentation and data module)
- Base (shared util, ext, etc)

## Dependencies

| Components                                  | Description                            |
|---------------------------------------------|----------------------------------------|
| androidx (appconpat, lifecycle, viewmodels) | Base framework                         |
| kotlin coroutines                           | Lightweight async framework for kotlin |
| retrofit                                    | Http connection library                |
| hilt (dagger)                               | Dependency Injection library           |
| room                                        | Local data storage library (sqlite)    |
| shimmer                                     | loading ui libs from facebook          |
| junit4                                      | base testing library                   |
| mockk                                       | mocking library for kotlin             |

## How to build

```bash
  ./gradlew clean assembleDebug
```

## How to run unit test

```bash
  ./gradlew clean test
```
