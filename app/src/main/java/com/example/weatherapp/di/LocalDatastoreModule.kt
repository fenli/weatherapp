package com.example.weatherapp.di

import android.content.Context
import androidx.room.Room
import com.example.weatherapp.features.places.data.local.PlacesDataStore
import com.example.weatherapp.local.WeatherAppDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object LocalDatastoreModule {

    @Provides
    @Singleton
    fun provideWeatherAppDB(@ApplicationContext appContext: Context): WeatherAppDB {
        return Room.databaseBuilder(
            appContext, WeatherAppDB::class.java, "WeatherAppDB"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun providePlacesDataStore(db: WeatherAppDB): PlacesDataStore = db
}
