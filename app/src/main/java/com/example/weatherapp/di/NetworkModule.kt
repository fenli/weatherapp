package com.example.weatherapp.di

import com.example.weatherapp.BuildConfig
import com.example.weatherapp.remote.ApiKeyInterceptor
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        )
        val client = OkHttpClient.Builder().addInterceptor(loggingInterceptor)
            .addInterceptor(ApiKeyInterceptor()).build()

        return Retrofit.Builder().baseUrl("https://api.openweathermap.org/").client(client)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create())).build()
    }
}
