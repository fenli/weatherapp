package com.example.weatherapp.di

import com.example.weatherapp.features.places.data.restservice.GeoLocationService
import com.example.weatherapp.features.weatherinfo.data.restservice.WeatherInfoService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RestAPIModule {

    @Singleton
    @Provides
    fun provideGeoLocationService(retrofit: Retrofit) =
        retrofit.create(GeoLocationService::class.java)

    @Singleton
    @Provides
    fun provideWeatherForecastService(retrofit: Retrofit) =
        retrofit.create(WeatherInfoService::class.java)
}
