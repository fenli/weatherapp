package com.example.weatherapp.di

import com.example.weatherapp.domain.repositories.PlacesRepository
import com.example.weatherapp.domain.repositories.WeatherInfoRepository
import com.example.weatherapp.features.places.data.PlacesRepositoryImpl
import com.example.weatherapp.features.weatherinfo.data.WeatherInfoRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun providePlacesRepository(repository: PlacesRepositoryImpl): PlacesRepository

    @Binds
    @Singleton
    abstract fun provideWeatherInfoRepository(repository: WeatherInfoRepositoryImpl): WeatherInfoRepository

}
