package com.example.weatherapp.di

import com.example.weatherapp.features.places.data.dao.FavoritePlacesDao
import com.example.weatherapp.local.WeatherAppDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DaoModule {

    @Provides
    @Singleton
    fun provideFavoritePlacesDao(usersDatabase: WeatherAppDB): FavoritePlacesDao {
        return usersDatabase.favoritePlacesDao
    }
}
