package com.example.weatherapp.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.weatherapp.features.places.data.dao.FavoritePlacesDao
import com.example.weatherapp.features.places.data.dtos.PlacesDBModel
import com.example.weatherapp.features.places.data.local.PlacesDataStore

@Database(entities = [PlacesDBModel::class], version = 1)
abstract class WeatherAppDB : RoomDatabase(), PlacesDataStore {
    abstract override val favoritePlacesDao: FavoritePlacesDao
}
