package com.example.weatherapp.features.weatherinfo.data.restservice

import com.example.weatherapp.features.weatherinfo.data.dtos.WeatherForecastResponseDto
import com.example.weatherapp.features.weatherinfo.data.dtos.WeatherResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherInfoService {

    @GET("data/2.5/weather")
    suspend fun getCurrentWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("units") units: String = "metric"
    ): WeatherResponseDto

    @GET("data/2.5/forecast")
    suspend fun getWeatherForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("units") units: String = "metric"
    ): WeatherForecastResponseDto
}
