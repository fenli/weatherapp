package com.example.weatherapp.features.weatherinfo.data

import com.example.weatherapp.base.isNextDays
import com.example.weatherapp.base.isTomorrow
import com.example.weatherapp.domain.entities.WeatherData
import com.example.weatherapp.features.weatherinfo.data.dtos.WeatherForecastResponseDto
import com.example.weatherapp.features.weatherinfo.data.dtos.WeatherResponseDto
import java.util.*

fun WeatherResponseDto.toWeatherData() = WeatherData(
    description = this.weather[0].description,
    dateTime = Date(this.dateTime * 1000),
    temperature = this.main.temperature,
    humidity = this.main.humidity,
    windSpeed = this.wind.speed,
    iconUrl = "https://openweathermap.org/img/wn/${this.weather[0].icon}@2x.png",
)

fun WeatherForecastResponseDto.toListOfWeatherData() = this.list
    .map { it.toWeatherData() }
    .filter { it.dateTime.run { isTomorrow() or isNextDays(2) or isNextDays(3) } }
    .distinctBy { it.dateTime.date }
