package com.example.weatherapp.features.weatherinfo.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.domain.TaskState
import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.domain.usecases.AddPlaceAsFavoriteUseCase
import com.example.weatherapp.domain.usecases.GetTodayWeatherUseCase
import com.example.weatherapp.domain.usecases.GetWeatherForecastUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class WeatherDetailViewModel @Inject constructor(
    private val getTodayWeather: GetTodayWeatherUseCase,
    private val getWeatherForecast: GetWeatherForecastUseCase,
    private val addPlaceAsFavorite: AddPlaceAsFavoriteUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow<ViewState>(ViewState.Init)
    val uiState: StateFlow<ViewState> = _uiState

    private val _favState = MutableSharedFlow<ViewState>()
    val favState: SharedFlow<ViewState> = _favState

    fun getWeatherDetail(lat: Double, lon: Double) {
        val location = GeoLocation(lat, lon)
        flowOf(
            getTodayWeather(location),
            getWeatherForecast(GetWeatherForecastUseCase.Param(location, 3))
        ).flattenMerge().onEach { taskState ->
            when (taskState) {
                is TaskState.InProgress -> {
                    _uiState.emit(ViewState.Loading)
                }
                is TaskState.Done<*> -> {
                    _uiState.emit(ViewState.WithData(taskState.data))
                }
                is TaskState.Failed -> {
                    _uiState.emit(ViewState.Error(taskState.message, taskState.error))
                }
            }
        }.launchIn(viewModelScope)
    }

    fun saveAsFavoritePlaces(city: String, country: String, lat: Double, lon: Double) {
        val location = GeoLocation(lat, lon)
        addPlaceAsFavorite(Place(city, country, location)).onEach { taskState ->
            when (taskState) {
                is TaskState.Done<*> -> {
                    _favState.emit(ViewState.WithData(true))
                }
                else -> Unit
            }
        }.launchIn(viewModelScope)
    }

}