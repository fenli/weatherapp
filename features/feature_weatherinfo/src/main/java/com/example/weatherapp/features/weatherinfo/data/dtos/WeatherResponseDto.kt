package com.example.weatherapp.features.weatherinfo.data.dtos

import com.google.gson.annotations.SerializedName
import java.util.*

data class WeatherResponseDto(
    @SerializedName("dt") val dateTime: Long,
    @SerializedName("main") val main: MainDto,
    @SerializedName("wind") val wind: WindDto,
    @SerializedName("weather") val weather: List<WeatherDto>,
) {

    data class MainDto(
        @SerializedName("temp") val temperature: Double,
        @SerializedName("humidity") val humidity: Double
    )

    data class WeatherDto(
        @SerializedName("description") val description: String,
        @SerializedName("icon") val icon: String
    )

    data class WindDto(@SerializedName("speed") val speed: Double)
}
