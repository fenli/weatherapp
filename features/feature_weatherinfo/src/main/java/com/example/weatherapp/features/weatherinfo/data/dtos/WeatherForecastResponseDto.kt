package com.example.weatherapp.features.weatherinfo.data.dtos

import com.google.gson.annotations.SerializedName
import java.util.*

data class WeatherForecastResponseDto(
    @SerializedName("list") val list: List<WeatherResponseDto>
)
