package com.example.weatherapp.features.weatherinfo.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.base.capitalizeWords
import com.example.weatherapp.base.observe
import com.example.weatherapp.domain.entities.WeatherData
import com.example.weatherapp.features.weatherinfo.R
import com.example.weatherapp.features.weatherinfo.databinding.ActivityWeatherDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeatherDetailActivity : AppCompatActivity() {

    private val vm: WeatherDetailViewModel by viewModels()
    private lateinit var binding: ActivityWeatherDetailBinding

    private val intentParams by lazy { IntentParams() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWeatherDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initToolbar()
        initContentView()
    }

    private fun initToolbar() {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        title = intentParams.city + ", " + intentParams.country
    }

    private fun initContentView() {
        observe(vm.uiState) {
            when (it) {
                is ViewState.Init -> vm.getWeatherDetail(intentParams.lat, intentParams.lon)
                is ViewState.Loading -> showLoading()
                is ViewState.WithData<*> -> {
                    val data = it.data
                    if (data is WeatherData) {
                        hideLoadingTodayData()
                        displayTodayWeatherData(data)
                    } else if (data is List<*> && data[0] is WeatherData) {
                        hideLoadingForecastData()
                        displayWeatherForecastData(data as List<WeatherData>)
                    }
                }
                else -> Unit
            }
        }
        observe(vm.favState) {
            when (it) {
                is ViewState.WithData<*> -> showToastMessage("Successfully added to favorite")
                else -> Unit
            }
        }
    }

    private fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun displayTodayWeatherData(todayData: WeatherData) {
        binding.mcvToday.visibility = View.VISIBLE
        binding.tvTodayWeatherCondition.setText(todayData.description.capitalizeWords())
        binding.tvTodayTemperature.setText(todayData.temperature.toString())
        binding.tvWindSpeeed.setText("Wind speed is ${todayData.windSpeed} m/s")
        binding.tvHumidity.setText("Humidity is ${todayData.humidity}")
        Glide.with(this).load(todayData.iconUrl).into(binding.ivWeatherIcon)
    }

    private fun displayWeatherForecastData(forecastData: List<WeatherData>) {
        binding.rvDailyForecast.visibility = View.VISIBLE
        binding.rvDailyForecast.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = DailyWeatherForecastRecyclerViewAdapter(forecastData)
        }
    }

    private fun showLoading() {
        binding.shimmerLayout.apply {
            startShimmer()
            visibility = View.VISIBLE
        }
        binding.shimmerLayout2.apply {
            startShimmer()
            visibility = View.VISIBLE
        }
    }

    private fun hideLoadingTodayData() {
        binding.shimmerLayout.apply {
            stopShimmer()
            visibility = View.GONE
        }
    }

    private fun hideLoadingForecastData() {
        binding.shimmerLayout2.apply {
            stopShimmer()
            visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.getItemId()) {
            R.id.add_favorite -> {
                addPlaceAsFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addPlaceAsFavorite() {
        vm.saveAsFavoritePlaces(
            intentParams.city, intentParams.country, intentParams.lat, intentParams.lon
        )
    }

    inner class IntentParams {
        val city = intent.getBundleExtra(BUNDLE_NAME)?.getString(BUNDLE_NAME_CITY).orEmpty()
        val country = intent.getBundleExtra(BUNDLE_NAME)?.getString(BUNDLE_NAME_COUNTRY).orEmpty()
        val lat = intent.getBundleExtra(BUNDLE_NAME)?.getDouble(BUNDLE_NAME_LAT) ?: 0.0
        val lon = intent.getBundleExtra(BUNDLE_NAME)?.getDouble(BUNDLE_NAME_LON) ?: 0.0
    }

    companion object {
        const val BUNDLE_NAME = "weather_detail_param"
        const val BUNDLE_NAME_CITY = "weather_detail_city"
        const val BUNDLE_NAME_COUNTRY = "weather_detail_country"
        const val BUNDLE_NAME_LAT = "weather_detail_latitude"
        const val BUNDLE_NAME_LON = "weather_detail_longitude"
    }
}
