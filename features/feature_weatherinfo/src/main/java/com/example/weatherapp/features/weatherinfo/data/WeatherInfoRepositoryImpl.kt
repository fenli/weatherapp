package com.example.weatherapp.features.weatherinfo.data

import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.repositories.WeatherInfoRepository
import com.example.weatherapp.features.weatherinfo.data.restservice.WeatherInfoService
import javax.inject.Inject

class WeatherInfoRepositoryImpl @Inject constructor(
    val remoteService: WeatherInfoService
) : WeatherInfoRepository {

    override suspend fun getTodayWeatherInfo(
        location: GeoLocation
    ) = remoteService.getCurrentWeather(location.latitude, location.longitude).toWeatherData()

    override suspend fun getWeatherForecastInfo(
        location: GeoLocation, days: Int
    ) = remoteService.getWeatherForecast(location.latitude, location.longitude)
        .toListOfWeatherData()

}
