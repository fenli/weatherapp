package com.example.weatherapp.features.weatherinfo.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.weatherapp.base.capitalizeWords
import com.example.weatherapp.base.formatAs
import com.example.weatherapp.domain.entities.WeatherData
import com.example.weatherapp.features.weatherinfo.databinding.ListItemDailyForecastBinding

class DailyWeatherForecastRecyclerViewAdapter(
    private val data: List<WeatherData>
) : RecyclerView.Adapter<DailyWeatherForecastRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemDailyForecastBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = data.size

    inner class ViewHolder(private val binding: ListItemDailyForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: WeatherData) {
            binding.tvDate.setText(item.dateTime.formatAs("E, MMM d"))
            binding.tvTodayWeatherCondition.setText(item.description.capitalizeWords())
            binding.tvTodayTemperature.setText(item.temperature.toString() + " \u2103")
            binding.tvWindSpeeed.setText("Wind: ${item.windSpeed} m/s")
            binding.tvHumidity.setText("Humidity: ${item.humidity}")
            Glide.with(binding.root).load(item.iconUrl).into(binding.ivWeatherIcon)
        }
    }
}