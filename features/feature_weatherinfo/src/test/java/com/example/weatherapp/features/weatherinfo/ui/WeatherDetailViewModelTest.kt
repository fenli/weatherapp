package com.example.weatherapp.features.weatherinfo.ui

import app.cash.turbine.test
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.entities.WeatherData
import com.example.weatherapp.domain.repositories.PlacesRepository
import com.example.weatherapp.domain.repositories.WeatherInfoRepository
import com.example.weatherapp.domain.usecases.AddPlaceAsFavoriteUseCase
import com.example.weatherapp.domain.usecases.GetTodayWeatherUseCase
import com.example.weatherapp.domain.usecases.GetWeatherForecastUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.*
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class WeatherDetailViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("main")

    @MockK
    private lateinit var weatherInfoRepositoryImpl: WeatherInfoRepository

    @MockK
    private lateinit var placesRepository: PlacesRepository

    private lateinit var vm: WeatherDetailViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this, relaxUnitFun = true)
        val getTodayWeatherUseCase = GetTodayWeatherUseCase(weatherInfoRepositoryImpl)
        val getWeatherForecastUseCase = GetWeatherForecastUseCase(weatherInfoRepositoryImpl)
        val addPlaceAsFavoriteUseCase = AddPlaceAsFavoriteUseCase(placesRepository)
        vm = WeatherDetailViewModel(
            getTodayWeatherUseCase,
            getWeatherForecastUseCase,
            addPlaceAsFavoriteUseCase
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Success get weather detail from rest api`() = runTest {
        val lat = 1.0
        val lon = 1.0
        val location = GeoLocation(lat, lon)
        val dt = System.currentTimeMillis() / 1000
        val weatherData = WeatherData("", Date(dt), 1.0, 1.0, 1.0, "")
        val weatherForecastData = listOf(weatherData)
        coEvery { weatherInfoRepositoryImpl.getTodayWeatherInfo(location) } returns weatherData
        coEvery {
            weatherInfoRepositoryImpl.getWeatherForecastInfo(
                location,
                any()
            )
        } returns weatherForecastData

        vm.uiState.test(timeout = 5.toDuration(DurationUnit.SECONDS)) {
            awaitItem() shouldBeEqualTo ViewState.Init

            vm.getWeatherDetail(lat, lon)

            awaitItem() shouldBeEqualTo ViewState.Loading

            val item1 = awaitItem()
            item1 shouldBeInstanceOf ViewState.WithData::class
            val item1Data = (item1 as ViewState.WithData<*>).data
            if (item1Data is WeatherData) {
                item1Data shouldBeEqualTo weatherData
            } else {
                item1Data shouldBeEqualTo weatherForecastData
            }

            val item2 = awaitItem()
            item2 shouldBeInstanceOf ViewState.WithData::class
            val item2Data = (item2 as ViewState.WithData<*>).data
            if (item2Data is WeatherData) {
                item2Data shouldBeEqualTo weatherData
            } else {
                item2Data shouldBeEqualTo weatherForecastData
            }

            ensureAllEventsConsumed()
        }
    }
}
