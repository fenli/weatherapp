package com.example.weatherapp.features.places.ui

import app.cash.turbine.test
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.domain.usecases.SearchPlacesUseCase
import com.example.weatherapp.features.places.data.PlacesRepositoryImpl
import com.example.weatherapp.features.places.data.dtos.PlaceResponseDto
import com.example.weatherapp.features.places.data.restservice.GeoLocationService
import com.example.weatherapp.features.places.data.toPlace
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.After
import org.junit.Before
import org.junit.Test

class SearchPlacesViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("main")

    @MockK
    private lateinit var geoLocationService: GeoLocationService

    private lateinit var vm: SearchPlacesViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this, relaxUnitFun = true)
        val repository = PlacesRepositoryImpl(geoLocationService, mockk())
        val usecase = SearchPlacesUseCase(repository)
        vm = SearchPlacesViewModel(usecase)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Success search places from rest service`() = runTest {
        val mockPlacesResponse = listOf(
            PlaceResponseDto("Jakarta", 1.0, 1.0, "ID"),
            PlaceResponseDto("Jakarta 2", 1.1, 1.1, "ID"),
            PlaceResponseDto("Jakarta 3", 1.2, 1.2, "ID"),
        )
        val searchQuery = "Jakarta"
        coEvery {
            geoLocationService.searchLocationByCityName(
                searchQuery,
                any()
            )
        } returns mockPlacesResponse

        val expectedData = mockPlacesResponse.map { it.toPlace() }

        vm.uiState.test {
            awaitItem() shouldBeEqualTo ViewState.Init

            vm.findPlacesByKeyword(searchQuery)

            awaitItem() shouldBeEqualTo ViewState.Loading

            val item = awaitItem()
            item shouldBeInstanceOf ViewState.WithData::class
            val data = (item as ViewState.WithData<*>).data as List<Place>
            data shouldBeEqualTo expectedData

            ensureAllEventsConsumed()
        }
    }
}