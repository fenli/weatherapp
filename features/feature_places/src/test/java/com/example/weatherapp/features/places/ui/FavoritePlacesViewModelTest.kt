package com.example.weatherapp.features.places.ui

import app.cash.turbine.test
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.domain.usecases.GetAllFavoritePlacesUseCase
import com.example.weatherapp.features.places.data.PlacesRepositoryImpl
import com.example.weatherapp.features.places.data.dao.FavoritePlacesDao
import com.example.weatherapp.features.places.data.dtos.PlacesDBModel
import com.example.weatherapp.features.places.data.local.PlacesDataStore
import com.example.weatherapp.features.places.data.restservice.GeoLocationService
import com.example.weatherapp.features.places.data.toPlace
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.After
import org.junit.Before
import org.junit.Test


class FavoritePlacesViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("main")

    @MockK
    private lateinit var geoLocationService: GeoLocationService

    @MockK
    private lateinit var placesDataStore: PlacesDataStore

    @MockK
    private lateinit var favoritePlacesDao: FavoritePlacesDao

    private lateinit var vm: FavoritePlacesViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this, relaxUnitFun = true)
        every { placesDataStore.favoritePlacesDao } returns favoritePlacesDao
        val repository = PlacesRepositoryImpl(geoLocationService, placesDataStore)
        val usecase = GetAllFavoritePlacesUseCase(repository)
        vm = FavoritePlacesViewModel(usecase)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Success load favorite from local datastore`() = runTest {
        val mockFavoritePlacesRows = listOf(
            PlacesDBModel(1.0, 1.0, "City", "Country"),
            PlacesDBModel(1.0, 1.0, "City", "Country"),
            PlacesDBModel(1.0, 1.0, "City", "Country")
        )
        coEvery { favoritePlacesDao.getAllFavoritePlaces() } returns mockFavoritePlacesRows

        val expectedData = mockFavoritePlacesRows.map { it.toPlace() }

        vm.uiState.test {
            awaitItem() shouldBeEqualTo ViewState.Init

            vm.loadAllFavoritesPlaces()

            awaitItem() shouldBeEqualTo ViewState.Loading

            val item = awaitItem()
            item shouldBeInstanceOf ViewState.WithData::class
            val data = (item as ViewState.WithData<*>).data as List<Place>
            data shouldBeEqualTo expectedData

            ensureAllEventsConsumed()
        }
    }
}