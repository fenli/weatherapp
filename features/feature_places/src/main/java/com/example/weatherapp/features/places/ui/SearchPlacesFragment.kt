package com.example.weatherapp.features.places.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.base.observe
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.features.places.databinding.FragmentSearchPlacesBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * A fragment representing a list of Items.
 */
@AndroidEntryPoint
class SearchPlacesFragment : Fragment() {

    private val vm: SearchPlacesViewModel by viewModels()
    private var _binding: FragmentSearchPlacesBinding? = null
    private val binding get() = _binding!!
    private val listAdapter = PlacesListRecyclerViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchPlacesBinding.inflate(inflater, container, false)
        val view = binding.root
        initView()
        return view
    }

    private fun initView() {
        binding.list.apply {
            layoutManager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(getContext(), LinearLayout.VERTICAL)
            addItemDecoration(dividerItemDecoration)
            listAdapter.onIntemClick = {
                openWeatherDetail(it)
            }
            adapter = listAdapter
        }
        binding.svPlaces.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(keyword: String?): Boolean {
                vm.findPlacesByKeyword(keyword ?: "")
                return true
            }

            override fun onQueryTextChange(keyword: String?) = false

        })
        observe(vm.uiState) {
            when (it) {
                is ViewState.Loading -> showLoading()
                is ViewState.WithData<*> -> {
                    hideLoading()
                    listAdapter.setItem(it.data as List<Place>)
                }
                else -> Unit
            }
        }
    }

    private fun openWeatherDetail(place: Place) {
        val intent = Intent("com.example.weatherapp.SHOW_WEATHER_DETAIL")
        intent.putExtra("weather_detail_param", Bundle().apply {
            putString("weather_detail_city", place.cityName)
            putString("weather_detail_country", place.country)
            putDouble("weather_detail_latitude", place.geoLocation.latitude)
            putDouble("weather_detail_longitude", place.geoLocation.longitude)
        })
        startActivity(intent)
    }

    private fun showLoading() {
        binding.shimmerLayout.apply {
            startShimmer()
            visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        binding.shimmerLayout.apply {
            stopShimmer()
            visibility = View.GONE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = SearchPlacesFragment()
    }
}
