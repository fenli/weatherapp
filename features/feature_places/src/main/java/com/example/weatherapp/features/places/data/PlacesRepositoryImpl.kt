package com.example.weatherapp.features.places.data

import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.domain.repositories.PlacesRepository
import com.example.weatherapp.features.places.data.local.PlacesDataStore
import com.example.weatherapp.features.places.data.restservice.GeoLocationService
import javax.inject.Inject

class PlacesRepositoryImpl @Inject constructor(
    val remoteService: GeoLocationService,
    val localDataStore: PlacesDataStore
) : PlacesRepository {

    override suspend fun findPlacesByCityName(cityName: String): List<Place> {
        return remoteService.searchLocationByCityName(cityName, 15).map { it.toPlace() }
    }

    override suspend fun savePlacesAsFavorites(place: Place) {
        localDataStore.favoritePlacesDao.addFavoritePlace(place.toPlaceDbModel())
    }

    override suspend fun loadFavoritePlaces(): List<Place> {
        return localDataStore.favoritePlacesDao.getAllFavoritePlaces().map { it.toPlace() }
    }
}
