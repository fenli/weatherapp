package com.example.weatherapp.features.places.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weatherapp.features.places.data.dtos.PlacesDBModel

@Dao
interface FavoritePlacesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addFavoritePlace(placesModel: PlacesDBModel)

    @Query("select * from favorite_places")
    suspend fun getAllFavoritePlaces(): List<PlacesDBModel>
}
