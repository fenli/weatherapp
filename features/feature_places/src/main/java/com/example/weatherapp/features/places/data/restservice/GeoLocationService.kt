package com.example.weatherapp.features.places.data.restservice

import com.example.weatherapp.features.places.data.dtos.PlaceResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface GeoLocationService {

    @GET("geo/1.0/direct")
    suspend fun searchLocationByCityName(
        @Query("q") q: String,
        @Query("limit") limit: Int
    ): List<PlaceResponseDto>
}
