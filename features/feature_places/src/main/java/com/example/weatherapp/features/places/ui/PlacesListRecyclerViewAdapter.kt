package com.example.weatherapp.features.places.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.features.places.databinding.ListItemGeoPlacesBinding

class PlacesListRecyclerViewAdapter :
    RecyclerView.Adapter<PlacesListRecyclerViewAdapter.ViewHolder>() {

    private val data: MutableList<Place> = arrayListOf()
    var onIntemClick: ((place: Place) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemGeoPlacesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    fun setItem(places: List<Place>) {
        val oldSize = data.size
        if (oldSize > 0) {
            data.clear()
            notifyItemRangeRemoved(0, oldSize)
        }

        val newSize = places.size
        if (newSize > 0) {
            data.addAll(places)
            notifyItemRangeInserted(0, newSize)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.contentView.text = "${item.cityName}, ${item.country}"
        holder.contentView.setOnClickListener { onIntemClick?.invoke(item) }
    }

    override fun getItemCount(): Int = data.size

    inner class ViewHolder(binding: ListItemGeoPlacesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val contentView: TextView = binding.content

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}