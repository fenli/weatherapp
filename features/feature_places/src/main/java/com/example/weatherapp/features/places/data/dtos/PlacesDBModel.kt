package com.example.weatherapp.features.places.data.dtos

import androidx.room.Entity

@Entity(tableName = "favorite_places", primaryKeys = ["lat", "lon"])
data class PlacesDBModel(
    val lat: Double,
    val lon: Double,
    val city: String,
    val country: String,
)
