package com.example.weatherapp.features.places.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.base.ViewState
import com.example.weatherapp.domain.TaskState
import com.example.weatherapp.domain.usecases.GetAllFavoritePlacesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class FavoritePlacesViewModel @Inject constructor(
    private val getAllFavoritePlaces: GetAllFavoritePlacesUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow<ViewState>(ViewState.Init)
    val uiState: StateFlow<ViewState> = _uiState

    fun loadAllFavoritesPlaces() {
        getAllFavoritePlaces().onEach { taskState ->
            when (taskState) {
                is TaskState.InProgress -> {
                    _uiState.emit(ViewState.Loading)
                }
                is TaskState.Done<*> -> {
                    _uiState.emit(ViewState.WithData(taskState.data))
                }
                is TaskState.Failed -> {
                    _uiState.emit(ViewState.Error(taskState.message))
                }
            }
        }.launchIn(viewModelScope)
    }
}