package com.example.weatherapp.features.places.data.dtos

import com.google.gson.annotations.SerializedName

data class PlaceResponseDto(
    @SerializedName("name") val cityName: String,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lon: Double,
    @SerializedName("country") val country: String,
)
