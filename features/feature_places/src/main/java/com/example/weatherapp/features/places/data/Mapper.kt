package com.example.weatherapp.features.places.data

import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.features.places.data.dtos.PlaceResponseDto
import com.example.weatherapp.features.places.data.dtos.PlacesDBModel

fun PlaceResponseDto.toPlace() = Place(
    this.cityName, this.country, GeoLocation(this.lat, this.lon)
)

fun PlacesDBModel.toPlace() = Place(
    this.city, this.country, GeoLocation(this.lat, this.lon)
)

fun Place.toPlaceDbModel() = PlacesDBModel(
    lat = this.geoLocation.latitude,
    lon = this.geoLocation.longitude,
    city = this.cityName,
    country = this.country
)