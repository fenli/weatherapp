package com.example.weatherapp.features.places.data.local

import com.example.weatherapp.features.places.data.dao.FavoritePlacesDao

interface PlacesDataStore {
    val favoritePlacesDao: FavoritePlacesDao
}
