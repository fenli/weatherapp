package com.example.weatherapp.domain

sealed class TaskState {
    class Done<T>(val data: T) : TaskState()
    class Failed(val message: String, val error: Throwable? = null) : TaskState()
    object InProgress : TaskState()
}
