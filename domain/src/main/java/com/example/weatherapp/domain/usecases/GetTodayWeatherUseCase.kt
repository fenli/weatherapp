package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.TaskState
import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.repositories.WeatherInfoRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetTodayWeatherUseCase @Inject constructor(
    val repository: WeatherInfoRepository
) {

    operator fun invoke(location: GeoLocation) = flow {
        try {
            emit(TaskState.InProgress)
            delay(1500)
            val result = repository.getTodayWeatherInfo(location)
            emit(TaskState.Done(result))
        } catch (e: Exception) {
            emit(TaskState.Failed("Something wrong!", e))
        }
    }
}
