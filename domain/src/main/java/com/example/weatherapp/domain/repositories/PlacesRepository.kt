package com.example.weatherapp.domain.repositories

import com.example.weatherapp.domain.entities.Place

interface PlacesRepository {

    suspend fun findPlacesByCityName(cityName: String): List<Place>

    suspend fun savePlacesAsFavorites(place: Place)

    suspend fun loadFavoritePlaces(): List<Place>
}
