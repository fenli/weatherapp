package com.example.weatherapp.domain.repositories

import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.entities.WeatherData

interface WeatherInfoRepository {

    suspend fun getTodayWeatherInfo(location: GeoLocation): WeatherData

    suspend fun getWeatherForecastInfo(location: GeoLocation, days: Int): List<WeatherData>
}
