package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.TaskState
import com.example.weatherapp.domain.entities.Place
import com.example.weatherapp.domain.repositories.PlacesRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AddPlaceAsFavoriteUseCase @Inject constructor(
    val repository: PlacesRepository
) {
    operator fun invoke(param: Place) = flow {
        try {
            emit(TaskState.InProgress)
            val result = repository.savePlacesAsFavorites(param)
            emit(TaskState.Done(result))
        } catch (e: Exception) {
            emit(TaskState.Failed("Something wrong!", e))
        }
    }
}
