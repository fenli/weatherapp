package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.TaskState
import com.example.weatherapp.domain.repositories.PlacesRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetAllFavoritePlacesUseCase @Inject constructor(
    val repository: PlacesRepository
) {

    operator fun invoke() = flow {
        try {
            emit(TaskState.InProgress)
            val places = repository.loadFavoritePlaces()
            emit(TaskState.Done(places))
        } catch (e: Exception) {
            emit(TaskState.Failed("Something wrong!", e))
        }
    }
}
