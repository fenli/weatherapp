package com.example.weatherapp.domain.entities

import java.util.*

data class WeatherData(
    val description: String,
    val dateTime: Date,
    val temperature: Double,
    val humidity: Double,
    val windSpeed: Double,
    val iconUrl: String
)
