package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.TaskState
import com.example.weatherapp.domain.entities.GeoLocation
import com.example.weatherapp.domain.repositories.WeatherInfoRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetWeatherForecastUseCase @Inject constructor(
    val repository: WeatherInfoRepository
) {

    data class Param(val location: GeoLocation, val days: Int)

    operator fun invoke(param: GetWeatherForecastUseCase.Param) = flow {
        try {
            emit(TaskState.InProgress)
            delay(1500)
            val result = repository.getWeatherForecastInfo(param.location, param.days)
            emit(TaskState.Done(result))
        } catch (e: Exception) {
            emit(TaskState.Failed("Something wrong!", e))
        }
    }
}
