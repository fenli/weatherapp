package com.example.weatherapp.domain.entities

data class GeoLocation(val latitude: Double, val longitude: Double)