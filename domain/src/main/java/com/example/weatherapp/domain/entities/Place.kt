package com.example.weatherapp.domain.entities

data class Place(
    val cityName: String,
    val country: String,
    val geoLocation: GeoLocation
)
