package com.example.weatherapp.base

sealed class ViewState {
    object Init : ViewState()
    object Loading : ViewState()
    class WithData<T>(val data: T) : ViewState()
    class Error(val message: String, val error: Throwable? = null) : ViewState()
}
