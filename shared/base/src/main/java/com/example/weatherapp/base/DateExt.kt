package com.example.weatherapp.base

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*

fun Date.formatAs(pattern: String): String {
    val dateFormat = SimpleDateFormat(pattern)
    return dateFormat.format(this)
}

fun Date.isTomorrow(): Boolean {
    return DateUtils.isToday(this.time - DateUtils.DAY_IN_MILLIS)
}

fun Date.isNextDays(days: Int): Boolean {
    return DateUtils.isToday(this.time - DateUtils.DAY_IN_MILLIS * days)
}