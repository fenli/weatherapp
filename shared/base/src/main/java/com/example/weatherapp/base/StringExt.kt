package com.example.weatherapp.base

fun String.capitalizeWords(): String = split(" ")
    .map { it.capitalize() }
    .joinToString(" ")
